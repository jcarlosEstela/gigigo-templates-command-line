//
//  Arguments.swift
//  gigigo-release
//
//  Created by José Estela on 2/3/17.
//  Copyright © 2017 José Estela. All rights reserved.
//

import Foundation

struct Arguments {
    
    /// Call this method to validate program arguments
    ///
    /// - Parameter args: args of program
    /// - Returns: if validated and error
    static func validateArgs(args: [String]) -> (validated: Bool, error: String?) {
        return rules(for: args) ? (validated: true, error: nil) : (validated: false, error: help())
    }
    
    /// Show help of program
    ///
    /// - Returns: The help
    static func help() -> String {
        return "Gigigo templates usage\n\n- download [branch]: To download and install templates with the branch setted (default is master)"
    }
}

private extension Arguments {
    
    /// Add custom rules to check arguments
    ///
    /// - Parameter args: args of program
    /// - Returns: if validated
    static func rules(for args: [String]) -> Bool {
        if args.count == 2 || args.count == 3 {
            switch args[1] {
            case "download":
                return true
            default:
                return false
            }
        }
        return false
    }
}
