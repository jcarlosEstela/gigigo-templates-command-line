//
//  Git.swift
//  gigigo-templates
//
//  Created by José Estela on 2/3/17.
//  Copyright © 2017 José Estela. All rights reserved.
//

import Foundation

struct Git {
    
    static func clone(repository: String, branch: String? = nil) {
        var args = ["clone"]
        if let branch = branch {
            args.append("-b")
            args.append(branch)
            args.append("--single-branch")
            args.append(repository)
        } else {
            args.append(repository)
        }
        System.execute(command: "/usr/bin/git", with: args)
    }
    
    static func pull(repository: String, branch: String, at path: String) {
        let filePath = path + "pull.sh"
        if (FileManager.default.createFile(atPath: filePath, contents: nil, attributes: nil)) {
            let pull = "#!/bin/bash\ncd '\(path)'\ngit pull \(repository) \(branch)"
            try? pull.write(toFile: filePath, atomically: true, encoding: .utf8)
        }
        System.execute(command: "/bin/sh", with: [filePath])
        System.removeItem(at: filePath)
    }
}
