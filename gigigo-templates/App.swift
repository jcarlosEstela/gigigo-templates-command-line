//
//  App.swift
//  gigigo-release
//
//  Created by José Estela on 28/2/17.
//  Copyright © 2017 José Estela. All rights reserved.
//

import Foundation

enum AppOption: String {
    case unknown
    case download
}

struct App {
    
    static let path = System.completePath(path: "~/Library/Developer/Xcode/Templates/")
    
    /// Command line program secuence
    ///
    /// - Parameter args: args of app
    static func run(args: [String]? = nil) {
        LogManager.shared.logLevel = .debug
        guard let args = args else { exit(1) }
        let argsValidation = Arguments.validateArgs(args: args)
        if argsValidation.validated {
            start(args: args)
        } else {
            if let error = argsValidation.error {
                LogError(error)
            }
            exit(1)
        }
    }
    
    private static func start(args: [String]) {
        Log("Starting Gigigo Templates")
        switch appOption(for: args[1]) {
        case .download:
            Log("Cloning from git")
            // Remove ios-templates if exist
            System.removeItem(at: "ios-templates")
            var branch = "master"
            if args.indices.contains(2) {
                branch = args[2]
            }
            Git.clone(
                repository: "https://github.com/gigigoapps/ios-templates",
                branch: branch
            )
            // Move Files
            let filesExist = System.exist(path: "ios-templates/File Templates")
            if filesExist {
                Log("Creating file templates")
                let fileDir = self.path + "File Templates/Gigigo Templates"
                System.createDirectory(at: fileDir)
                System.moveItem(at: "ios-templates/File Templates", to: fileDir)
            } else {
                LogWarn("No file templates found!")
            }
            // Move Projects
            let projectsExist = System.exist(path: "ios-templates/Project Templates")
            if projectsExist {
                Log("Creating project templates")
                let projectDir = self.path + "Project Templates/Gigigo Templates"
                System.createDirectory(at: projectDir)
                System.moveItem(at: "ios-templates/Project Templates", to: projectDir)
            } else {
                LogWarn("No project templates found!")
            }
            System.removeItem(at: "ios-templates")
        case .unknown:
            break
        }
        Log("Finished process")
    }
    
    private static func appOption(for option: String) -> AppOption {
        if option == "download" {
            return .download
        }
        return .unknown
    }
}
