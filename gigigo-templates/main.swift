//
//  main.swift
//  gigigo-templates
//
//  Created by José Estela on 2/3/17.
//  Copyright © 2017 José Estela. All rights reserved.
//

import Foundation

// Perform the application
App.run(args: CommandLine.arguments)

